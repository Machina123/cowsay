#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay -f tux
else
   /usr/games/cowsay -f vader "$@"
fi
